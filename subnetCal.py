import ipaddress

def subnet_calculator():
    # 1. รับค่า Network Class, IP Address, Subnet Mask, และ Subnet Bits จากผู้ใช้
    network_class = input("กรุณาเลือก Network Class (A, B, C): ").upper()
    ip_address = input("กรอก IP Address: ")
    subnet_mask = input("กรอก Subnet Mask: ")
    subnet_bits = int(input("กรอกจำนวน Subnet Bits: "))

    # 2. คำนวณ Subnet Mask จาก IP Address และ Subnet Mask ที่ผู้ใช้ป้อน
    subnet = ipaddress.IPv4Network(f"{ip_address}/{subnet_mask}", strict=False)
    subnet_mask = str(subnet.netmask)

    # 3. คำนวณ Maximum Subnets จากจำนวน Subnet Bits ที่ผู้ใช้ป้อน
    max_subnets = 2 ** subnet_bits

    # 4. คำนวณ First Octet Range โดยตรวจสอบ Network Class
    if network_class == 'A':
        first_octet_range = f"1 - 126"
    elif network_class == 'B':
        first_octet_range = f"128 - 191"
    elif network_class == 'C':
        first_octet_range = f"192 - 223"
    else:
        first_octet_range = "N/A"

    # 5. แปลง IP Address เป็นรูป Hexadecimal
    hex_ip_address = ipaddress.IPv4Address(ip_address).exploded

    # 6. คำนวณ Wildcard Mask โดยใช้ hostmask ของ Subnet
    wildcard_mask = subnet.hostmask

    # 7. คำนวณ Mask Bits โดยใช้ Subnet Mask
    mask_bits = subnet_mask

    # 8. คำนวณ Hosts per Subnet โดยใช้ความต่างระหว่าง 32 และ prefix length ของ Subnet
    hosts_per_subnet = 2 ** (32 - subnet.prefixlen)

    # 9. คำนวณ Host Address Range โดยคำนวณที่อยู่แรกและที่อยู่สุดท้ายใน Subnet
    first_host = subnet.network_address + 1
    last_host = subnet.broadcast_address - 1
    host_address_range = f"{first_host} - {last_host}"

    # 10. คำนวณ Subnet ID โดยใช้ network address ของ Subnet

    # 11. แสดงผลลัพธ์การคำนวณ Subnet บนหน้าจอ

    print("\nผลลัพธ์การคำนวณ Subnet:")
    print(f"Network Class: {network_class}")
    print(f"Subnet Mask: {subnet_mask}")
    print(f"Subnet Bits: {subnet_bits}")
    print(f"Maximum Subnets: {max_subnets}")
    print(f"First Octet Range: {first_octet_range}")
    print(f"Hex IP Address: {hex_ip_address}")
    print(f"Wildcard Mask: {wildcard_mask}")
    print(f"Mask Bits: {mask_bits}")
    print(f"Hosts per Subnet: {hosts_per_subnet}")
    print(f"Host Address Range: {host_address_range}")
    print(f"Subnet ID: {subnet.network_address}")

# 12. เรียกใช้ฟังก์ชัน subnet_calculator เมื่อโปรแกรมรัน
if __name__ == "__main__":
    subnet_calculator()